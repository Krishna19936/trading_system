package com.example.trading.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.trading.dto.StockDTO;
import com.example.trading.entity.UserTransac;
import com.example.trading.exception.InvalidUserActionException;
import com.example.trading.exception.NotEnoughStocksException;
import com.example.trading.exception.TransactionNotFoundException;
import com.example.trading.feignclient.StockPrice;
import com.example.trading.service.UserService;
import com.example.trading.service.UserTransactionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@EnableFeignClients
@RequestMapping("/")
@Api(value = "TradingController")
public class TradingController {
	
	@Autowired
    private UserService userservice;

 

    @Autowired
    private StockPrice stockPrice;
	
	
	
	@Autowired
    private UserTransactionService userTransactionService;
	
	
	
	
	@PostMapping(path = "/user-actions/{userId}")
	@ApiOperation("User buy or sell stock")
	private ResponseEntity<UserTransac> userBuySellStocks(
			@RequestBody UserTransac usert) throws NotEnoughStocksException,InvalidUserActionException {
		
		UserTransac usertransac = userTransactionService.userStockAction(usert);
		return ResponseEntity.status(HttpStatus.OK).body(usertransac);
		
	}

	
	@GetMapping("/user-transactions/{userId}")
	@ApiOperation("fetch stocks bought sold in past 24 hrs")
	public ResponseEntity<List<UserTransac>> getUserLatestTransactions(@PathVariable("userId") long userId) throws TransactionNotFoundException {
		
		    return new ResponseEntity<>(userTransactionService.fetchalltransactions(userId), HttpStatus.OK);
		
	}
	
	/**
     * Method for login
     * 
     * @param userId
     * @param password
     * @return message
     */
    @ApiOperation("Login to application")
    @GetMapping(value = "/login", produces = "application/json")
    public ResponseEntity<String> login(@RequestHeader("userId") Long userId,
            @RequestHeader("password") String password) {

 

        String message = userservice.login(userId, password);
        return ResponseEntity.status(HttpStatus.OK).body(message);
    }
    
    /**
     * 
     * @param Stock price
     * @return
     */

 

    @GetMapping("/stocks")
    @ApiOperation("fetch  AllStocks")
    public ResponseEntity<List<StockDTO>> getAllStocks() {


        return new ResponseEntity<>(stockPrice.fetchallstocks(), HttpStatus.OK);

    }
    
    
   
	
	
}
