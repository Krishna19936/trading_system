package com.example.trading.dto;

public class StockDTO {
	
	 private int stockId;
	 private String stockName;
	 private double stockPrice;
	 private String diffPrevDay;
	 private String trend;
	 private long quantity;
	 
	public StockDTO() {
		super();
		
	}
	public StockDTO(int stockId, String stockName, double stockPrice, String diffPrevDay, String trend, long quantity) {
		super();
		this.stockId = stockId;
		this.stockName = stockName;
		this.stockPrice = stockPrice;
		this.diffPrevDay = diffPrevDay;
		this.trend = trend;
		this.quantity = quantity;
	}
	public int getStockId() {
		return stockId;
	}
	public void setStockId(int stockId) {
		this.stockId = stockId;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public double getStockPrice() {
		return stockPrice;
	}
	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}
	public String getDiffPrevDay() {
		return diffPrevDay;
	}
	public void setDiffPrevDay(String diffPrevDay) {
		this.diffPrevDay = diffPrevDay;
	}
	public String getTrend() {
		return trend;
	}
	public void setTrend(String trend) {
		this.trend = trend;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	    
	 

}
