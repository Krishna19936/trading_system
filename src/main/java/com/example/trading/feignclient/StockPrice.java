package com.example.trading.feignclient;
import java.util.List;
import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;


import com.example.trading.dto.StockDTO;

@FeignClient(value = "stock1", url = "http://localhost:8090/")
public interface StockPrice {
	
	 @GetMapping("/stocks")
	    public List<StockDTO> fetchallstocks();
	


	 @PutMapping(value = "/{stockId}/{quantity}/{action}")
	 public void updateStockQuantity(@PathVariable("stockId") long stockId, @PathVariable("quantity") long quantity,  @PathVariable("action") String action);
	 
	 @GetMapping("/stockvalidation/{stockId}")
	    public Optional<StockDTO> validatestock(@PathVariable("stockId") long stockId);
}
