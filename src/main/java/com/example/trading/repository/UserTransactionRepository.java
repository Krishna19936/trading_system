package com.example.trading.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.trading.entity.UserTransac;




public interface UserTransactionRepository extends JpaRepository<UserTransac,Long> {

	
	
	@Query(value="SELECT action FROM user_transac WHERE stock_id = ?1 and user_id = ?2 and transaction_time >= NOW() - INTERVAL 1 DAY limit 1;",nativeQuery=true)
	public String checkuserLastTransaction(long stockId, long userId);
	
	@Query(value="SELECT * FROM user_transac WHERE user_id = ?1 and transaction_time >= NOW() - INTERVAL 1 DAY;",nativeQuery=true)
	public Optional<List<UserTransac>> getlatestusertransactions(long userId);
	
	
	
	
	

	
	
	

}
