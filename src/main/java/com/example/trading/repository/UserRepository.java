package com.example.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.trading.entity.User;


public interface UserRepository extends JpaRepository<User,Long> {
	
	User findByUserIdAndPassword(long userId, String password);

}
