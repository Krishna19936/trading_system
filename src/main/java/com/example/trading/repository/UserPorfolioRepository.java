package com.example.trading.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.trading.entity.UserPorfolio;

public interface UserPorfolioRepository extends JpaRepository<UserPorfolio,Long> {
	
	@Query(value="SELECT * FROM user_porfolio where user_id = ?1 and stock_id = ?2",nativeQuery=true)
	Optional<UserPorfolio> getuserporfolio(long userId, long stockId);
	
	
	@Transactional
	@Modifying
	@Query(value="Update user_porfolio set quantity_holding = ?1 where porfolio_id = ?2",nativeQuery=true)
	public int updateuserholding(long updatedquantity, long porfolioId);

}
