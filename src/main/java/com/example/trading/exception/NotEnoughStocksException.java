package com.example.trading.exception;

public class NotEnoughStocksException extends Exception{


	private static final long serialVersionUID = 1L;

	public NotEnoughStocksException() {

	}

	public NotEnoughStocksException(String message) {
		super(message);	
	}
}
