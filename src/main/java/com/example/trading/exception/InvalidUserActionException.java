package com.example.trading.exception;

public class InvalidUserActionException extends Exception{


	private static final long serialVersionUID = 1L;

	public InvalidUserActionException() {

	}

	public InvalidUserActionException(String message) {
		super(message);	
	}
}