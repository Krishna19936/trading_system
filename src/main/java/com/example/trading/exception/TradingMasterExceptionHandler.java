package com.example.trading.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;



@ControllerAdvice
public class TradingMasterExceptionHandler {
	
	  @ExceptionHandler(value = NotEnoughStocksException.class)
	   public ResponseEntity<Object> handleNotEnoughStocksException() {
		   return new ResponseEntity<>("Not enough stocks in your porfolio to sell", HttpStatus.NOT_FOUND);
	}

	    @ExceptionHandler(value = InvalidUserActionException.class)
		public ResponseEntity<Object> handleInvalidUserActionException() {
			return new ResponseEntity<>("Cannot perform this action", HttpStatus.NOT_FOUND);
		}
	    
	    @ExceptionHandler(value = TransactionNotFoundException.class)
		public ResponseEntity<Object> handleTransactionNotFoundException() {
			return new ResponseEntity<>("No Transaction found for this user or this is invalid user id", HttpStatus.NOT_FOUND);
		}


}
