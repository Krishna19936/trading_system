package com.example.trading.entity;


import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "UserTransac")
public class UserTransac {
	

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "transactionId")
	    private long transactionId;
		
		@Column(name = "userId")
	    private long userId;
		
		@Column(name = "stockId")
	    private long stockId;
		
		@Column(name = "quantity")
	    private long quantity;
		
		@Column(name="transactionTime")
		private LocalDateTime transactionTime = LocalDateTime.now();
		
		@Column(name = "action")
	    private String action;

		
		public LocalDateTime getTransactionTime() {
			return transactionTime;
		}

		public void setTransactionTime(LocalDateTime transactionTime) {
			this.transactionTime = transactionTime;
		}

		public long getTransactionId() {
			return transactionId;
		}

		public void setTransactionId(long transactionId) {
			this.transactionId = transactionId;
		}

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

		public long getStockId() {
			return stockId;
		}

		public void setStockId(long stockId) {
			this.stockId = stockId;
		}

		public long getQuantity() {
			return quantity;
		}

		public void setQuantity(long quantity) {
			this.quantity = quantity;
		}

	

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public UserTransac(long transactionId, long userId, long stockId, long quantity, LocalDateTime transactionTime,
				String action) {
		
			this.transactionId = transactionId;
			this.userId = userId;
			this.stockId = stockId;
			this.quantity = quantity;
			this.transactionTime = transactionTime;
			this.action = action;
		}

		public UserTransac(long userId, long stockId, long quantity, String action) {
			this.userId = userId;
			this.stockId = stockId;
			this.quantity = quantity;
			this.action = action;
		}
		public UserTransac() {
			super();
			
		}
		
		

	}



