package com.example.trading.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.trading.dto.StockDTO;
import com.example.trading.entity.UserTransac;
import com.example.trading.exception.InvalidUserActionException;
import com.example.trading.exception.NotEnoughStocksException;
import com.example.trading.exception.TransactionNotFoundException;
import com.example.trading.feignclient.StockPrice;
import com.example.trading.repository.UserTransactionRepository;

@Service
public class UserTransactionService {
	
	@Autowired
	private UserTransactionRepository userTransactionRepository;
	
	@Autowired
	private StockPrice stockPrice;
	
	@Autowired
	private UserPorfolioService userPorfolioservice;
	

	
	public UserTransac userStockAction(UserTransac usert) throws NotEnoughStocksException,InvalidUserActionException {
		
		
		long quantity = usert.getQuantity();
		long stockId = usert.getStockId();
		long userId = usert.getUserId();
		String action = usert.getAction();
		if(!usert.getAction().equalsIgnoreCase("buy") && !usert.getAction().equalsIgnoreCase("sell")) {
			throw new InvalidUserActionException();
		}
		String utl = userTransactionRepository.checkuserLastTransaction(stockId, userId);
		Optional<StockDTO> sp = stockPrice.validatestock(stockId);
		
		
		if((utl == null || utl.equals(action)) && sp.isPresent()) {
			
			
			UserTransac ut = new UserTransac();
			ut.setUserId(userId);
			ut.setStockId(stockId);
			ut.setQuantity(quantity);
			ut.setAction(action);
			userPorfolioservice.maintainUserPorfolio(userId, stockId, quantity, action);
			userTransactionRepository.save(ut);
			stockPrice.updateStockQuantity(stockId, quantity, action);
			
			
			
			return ut; 
		}
		
		else {
			
			throw new InvalidUserActionException();
		}
		
		
		
		
		
		
		
	
	}
	
	public List<UserTransac> fetchalltransactions(long userId) throws TransactionNotFoundException{
		
		Optional<List<UserTransac>> utlist = userTransactionRepository.getlatestusertransactions(userId);
		if(!utlist.isPresent()){
			
			throw new TransactionNotFoundException();
		}
		List<UserTransac> ut1 = utlist.get();
		if(ut1.isEmpty()) {
			throw new TransactionNotFoundException();
		}
		
		
		return ut1;
		
	}
	
	
}
