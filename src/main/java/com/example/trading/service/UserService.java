package com.example.trading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.trading.entity.User;
import com.example.trading.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
    private UserRepository userRepository;

 

    public String login(long userId, String password) {

 

        User user = userRepository.findByUserIdAndPassword(userId, password);
        if (user != null)
            return "Success";

 

        else
            return "Failed";

 

    }

}
