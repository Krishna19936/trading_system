package com.example.trading.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.trading.entity.UserPorfolio;
import com.example.trading.exception.NotEnoughStocksException;
import com.example.trading.repository.UserPorfolioRepository;


@Service
public class UserPorfolioService {
	
	
	
	@Autowired
	private UserPorfolioRepository userPorfolioRepository;
	
	public void maintainUserPorfolio(long userId, long stockId, long quantity, String action) throws NotEnoughStocksException{
	
	Optional<UserPorfolio> up = userPorfolioRepository.getuserporfolio(userId,stockId);
	
	if(!up.isPresent() && action.equals("sell")) {
		throw new NotEnoughStocksException();
	}
	
	if(up.isPresent()) {
		UserPorfolio userpor = up.get();
		
		if(action.equals("buy")) {
			
			long updatedQuantity = userpor.getQuantityHolding() + quantity;
			userpor.setQuantityHolding(updatedQuantity);
		}
		
		if(action.equals("sell")) {
			
			long updatedQuantity = userpor.getQuantityHolding() - quantity;
			if(updatedQuantity < 0) {
				throw new NotEnoughStocksException();
				}
			userpor.setQuantityHolding(updatedQuantity);
			}
		
		userPorfolioRepository.updateuserholding(userpor.getQuantityHolding(), userpor.getPorfolioId());
	}
	
	else {
		
		
		UserPorfolio userp = new UserPorfolio();
		userp.setQuantityHolding(quantity);
		userp.setStockId(stockId);
		userp.setUserId(userId);
		userPorfolioRepository.save(userp);
	}
	
	}

}
