package com.example.trading.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.trading.entity.UserPorfolio;
import com.example.trading.entity.UserTransac;
import com.example.trading.exception.InvalidUserActionException;
import com.example.trading.exception.NotEnoughStocksException;
import com.example.trading.exception.TransactionNotFoundException;
import com.example.trading.repository.UserPorfolioRepository;
import com.example.trading.repository.UserTransactionRepository;





@SpringBootTest
public class UserPorfolioServiceTest {
	
	@InjectMocks
	UserPorfolioService userporfolioService;

	@Mock
	UserTransactionRepository usertransactionRepository;
	
	@Mock
	UserPorfolioRepository userporfolioRepository;
	
	
	
	@Test
	public void TestmaintainUserPorfolio() throws InvalidUserActionException,NotEnoughStocksException  {
		
		UserPorfolio up = new UserPorfolio();
		up.setPorfolioId(1);
		up.setQuantityHolding(100);
		up.setStockId(2);
		up.setUserId(3);
		String action = new String("buy");
		long quantity = 10;
		when(userporfolioRepository.getuserporfolio(3,2)).thenReturn(Optional.of(up)); 
		when(userporfolioRepository.updateuserholding(110,1)).thenReturn(1);
		
	
		userporfolioService.maintainUserPorfolio(3, 2, quantity, action);
		
		int result1 = userporfolioRepository.updateuserholding(110,1);
		
		Optional<UserPorfolio> ut1 = userporfolioRepository.getuserporfolio(3,2);
			
		UserPorfolio result = ut1.get();
		assertEquals(110, result.getQuantityHolding());
		assertEquals(2, result.getStockId());
		assertEquals(3, result.getUserId());
		assertEquals(1, result1);
			
		
		
		
		
		
		
		
	}
	


}
