package com.example.trading.service;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

 

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

 

import com.example.trading.entity.User;
import com.example.trading.repository.UserRepository;

@SpringBootTest
public class UserServiceTest {
	
	@Mock
    UserRepository userRepository;
    
    @InjectMocks
    UserService userService;

 

    @BeforeEach
    public void init() {

 

    }
    
    @Test
    public void testLogin() {
        User user=new User();
        user.setUserId(1);
        user.setPassword("12345");
        user.setUserName("shiva");
        when(userRepository.findByUserIdAndPassword(1, "12345")).thenReturn(user);
        assertEquals("Success", userService.login(1, "12345"));
    }
    
    

 

    @Test
    public void testfaileLogin() {
        User user=new User();
        user.setUserId(1);
        user.setPassword("12345");
        user.setUserName("shiva");
        when(userRepository.findByUserIdAndPassword(1, "12345")).thenReturn(user);
        assertNotEquals("fail", userService.login(1, "1111"));
      
        // Do some other valuable testing
    }

}
