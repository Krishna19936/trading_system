package com.example.trading.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.trading.entity.UserTransac;
import com.example.trading.exception.InvalidUserActionException;
import com.example.trading.exception.NotEnoughStocksException;
import com.example.trading.exception.TransactionNotFoundException;
import com.example.trading.repository.UserPorfolioRepository;
import com.example.trading.repository.UserTransactionRepository;





@SpringBootTest
public class UserTransactionServiceTest {
	
	@InjectMocks
	UserTransactionService usertransactionService;
	
	@InjectMocks
	UserPorfolioService userporfolioService;

	@Mock
	UserTransactionRepository usertransactionRepository;
	
	@Mock
	UserPorfolioRepository userporfolioRepository;
	
	@Test
	public void Testfetchalltransactions()  throws TransactionNotFoundException,InvalidUserActionException{
		
		LocalDateTime transactionTime = LocalDateTime.now();
		UserTransac ut = new UserTransac();
		ut.setAction("buy");
		ut.setQuantity(10);
		ut.setStockId(1);
		ut.setUserId(2);
		ut.setTransactionTime(transactionTime);
		
		List<UserTransac> utt = new ArrayList<>();
		utt.add(ut);
		when(usertransactionRepository.getlatestusertransactions(2)).thenReturn(Optional.of(utt)); 
		
		List<UserTransac> ut3 = usertransactionService.fetchalltransactions(2);
		
		Optional<List<UserTransac>> ut1 = usertransactionRepository.getlatestusertransactions(2);
		
		UserTransac result = ut3.get(0);
		assertEquals(1, result.getStockId());
		assertEquals(10, result.getQuantity());
		
	
	}
	
	
	@Test
	public void TestuserStockAction() throws NotEnoughStocksException,InvalidUserActionException{
		
		LocalDateTime transactionTime = LocalDateTime.now();
		UserTransac ut = new UserTransac();
		ut.setAction("buy");
		ut.setQuantity(10);
		ut.setStockId(1);
		ut.setUserId(1);
		ut.setTransactionTime(transactionTime);
		
		 
		String action = new String("buy");
		when(usertransactionRepository.checkuserLastTransaction(1,1)).thenReturn("buy");
		String ut3 = usertransactionRepository.checkuserLastTransaction(1,1);
		
		UserTransac ut7 = usertransactionService.userStockAction(1, 1, 10, action);
		
		
		
		
	
		assertEquals(10, ut7.getQuantity());
		assertEquals(1, ut7.getStockId());
		assertEquals(1, ut7.getUserId());
		assertEquals("buy", ut3);
		
		
	}

}
